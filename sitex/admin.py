from django.contrib import admin

from sitex.models.building import Building


class BuildingAdmin(admin.ModelAdmin):
    pass


admin.site.register(Building, BuildingAdmin)
