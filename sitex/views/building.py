from sitex.serializers.building import serialize_building
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from sitex.models.building import Building


def get_building(request, id):
    instance = get_object_or_404(Building, id=id)
    return JsonResponse(serialize_building(instance))