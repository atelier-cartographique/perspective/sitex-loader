"""sitex URL Configuration

"""
from django.urls import path as base_path

from sitex.views.building import get_building


def path(route: str, *args, **kwargs):
    return base_path('sitex/' + route, *args, **kwargs)


urlpatterns = [
    path('building/<int:id>', get_building),
]
