from django.contrib.gis.db import models


class Building(models.Model):
    id = models.AutoField(primary_key=True)
